%========= Estructura de control general  =========

 resolverPuzzle([Pista|Pistas]):-Pista,resolverPuzzle(Pistas).
 resolverPuzzle([]).

 mostrar([]).
 mostrar([C|Cs]) :- writeln(C),mostrar(Cs).
 
 resolver() :-
          structura(Structura),
          pistas(Structura,Pistas),
          preguntas(Structura,Preguntas,Solucion),
          resolverPuzzle(Pistas),
          resolverPuzzle(Preguntas),
          writeln(""),
          writeln("Estructura resultado:"),  
          mostrar(Structura),
          writeln(""),
          writeln("Respuestas:"),             
          mostrar(Solucion),
          writeln(""),
          writeln(""). 
          
%========= Relaciones especificas de Reunion De Te  =========
  
structura([persona(_,_,_),
           persona(_,_,_),
           persona(_,_,_)]).

pistas( 
      
    ReunionDeTe,  
    [pistasReunionDeTe([1,2,3,4],ReunionDeTe)]).

preguntas(
         
        ReunionDeTe, 
        [ 
        select(persona(_,alicia,_),ReunionDeTe,E2),
        select(persona(A1,A2,A3),E2,E3),
        member(persona(B1,B2,B3),E3)
        ],
        [
            ['Las amigas de alicia son las siguientes'],
            ['La amiga uno se apellida ', A1  , ' el nombre ', A2, ' y se dedica a ', A3],
            ['La amiga dos se apellida ', B1  , ' el nombre ', B2, ' y se dedica a ', B3]
        ]).


 %========= Pistas de Reunion De Te =========

pistasReunionDeTe([],_).
pistasReunionDeTe([P|Ps],E) :-pistasReunionDeTe(P,E),pistasReunionDeTe(Ps,E).

pistasReunionDeTe(1,E) :-  select(persona(garcia,_A2,_A3),E,E2), select(persona(lopez,_B2,_B3),E2,E3), select(persona(mendez,_C2,_C3),E3,_).

pistasReunionDeTe(2,E) :- select(persona(garcia,_,_),E,E2), member(persona(_,beatriz,_), E2).

pistasReunionDeTe(3,E) :-  member(persona(lopez,_,secretariaOficina),E).

pistasReunionDeTe(4,E) :- select(persona(_,claudia,actriz),E,_), select(persona(mendez,_,_),E,E3), member(persona(_,_,maestra),E3).


